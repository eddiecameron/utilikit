/* DebugExtras.cs
 * ----------------------------
 * Copyright Eddie Cameron & Grasshopper 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 * 
 */

using UnityEngine;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Extends the Unity Debug class to compile and execute conditionally. Stack traces can easily eat up performance otherwise
/// DEBUG flag can be set in PlayerSettings > Scripting Define Symbols
/// </summary>
public static class DebugExtras 
{
    /// <summary>
    /// For other classes to check at runtime whether it is in debug mode
    /// </summary>
    /// <value><c>true</c> if debug build; otherwise, <c>false</c>.</value>
	public static bool debugBuild
	{
		get
		{
			bool debug = false;
#if DEBUG
    		debug = true;
#endif
			return debug;
		}
	}

    /// <summary>
    /// Raises an error if <paramref name="isTrue"/> evaluates to false
    /// </summary>
    /// <param name="isTrue"></param>
    /// <param name="assertMsg">Optional: Print msg if assert fails</param>
    [Conditional( "DEBUG" )]
    public static void Assert( bool isTrue, string assertMsg = null )
    {
        if ( !isTrue )
            UnityEngine.Debug.LogError( "Assert failed " + assertMsg ?? "" );
    }
    
    /// <summary>
    /// Log <paramref name="toLog"/> to console
    /// </summary>
    /// <param name="toLog"></param>
    /// <param name="context">Optional: What object caused the log</param>
    [Conditional( "DEBUG" )]
    public static void Log( object toLog, Object context = null)
    {
        UnityEngine.Debug.Log( toLog + "\n-----------------------", context );
    }

    /// <summary>
    /// Log <paramref name="toLog"/> as warning to console
    /// </summary>
    /// <param name="toLog"></param>
    /// <param name="context">Optional: What object caused the warning</param>
    [Conditional( "DEBUG" )]
    public static void LogWarning( object toLog, Object context = null)
    {
        UnityEngine.Debug.LogWarning( toLog + "\n-----------------------", context );
    }

    /// <summary>
    /// Log <paramref name="toLog"/> as error to console
    /// </summary>
    /// <param name="toLog"></param>
    /// <param name="context">Optional: What object caused the error</param>
    [Conditional( "DEBUG" )]
    public static void LogError( object toLog, Object context = null)
    {
        UnityEngine.Debug.LogError( toLog + "\n-----------------------", context );
    }
}
