﻿/* Follow.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FollowOnGrid : MonoBehaviour {
    public Transform target;
    public bool followX = true, followY = true, followZ = true;
    public Vector3 offset;
    public Vector3 gridSize;

    protected void Update() {
        if ( target ) {
            Vector3 newPos = transform.position;
            Vector3 targPos = target.position + offset;

            int jumps;
            if ( followX ) {
                jumps = (int)( ( targPos.x - newPos.x ) / gridSize.x );
                newPos.x += jumps * gridSize.x;
            }
            if ( followY ) {
                jumps = (int)( ( targPos.y - newPos.y ) / gridSize.y );
                newPos.y += jumps * gridSize.y;
            }
            if ( followZ ) {
                jumps = (int)( ( targPos.z - newPos.z ) / gridSize.z );
                newPos.z += jumps * gridSize.z;
            }

            transform.position = newPos;
        }
    }
}