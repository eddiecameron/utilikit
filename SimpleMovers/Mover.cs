﻿using UnityEngine;
using System.Collections;

namespace SimpleMovers {
public class Mover : MonoBehaviour {
    public Space space = Space.Self;
    public Vector3 velocity;

    protected void Update() {
        transform.Translate( velocity * Time.deltaTime, space );
    }
}
}
