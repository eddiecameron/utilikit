﻿/* Rotator.cs
 * Copyright Eddie Cameron 2014
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rotator : MonoBehaviour {
    public Axis axis;
    public float speed; // in deg per sec
    public float startOffset;

    protected void Start() {
        transform.Rotate( axis == Axis.X ? startOffset : 0,
                          axis == Axis.Y ? startOffset : 0,
                          axis == Axis.Z ? startOffset : 0 );
    }

    protected void Update() {
        transform.Rotate( axis == Axis.X ? speed * Time.deltaTime : 0f,
            axis == Axis.Y ? speed * Time.deltaTime : 0,
            axis == Axis.Z ? speed * Time.deltaTime : 0 );
    }

    public enum Axis {
        X, Y, Z
    }
}