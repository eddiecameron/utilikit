﻿/* Oscillator.cs
 * Copyright Eddie Cameron 2014
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SimpleMovers {
    public class Oscillator : MonoBehaviour {
        public float amplitude;
        public float period;
        public Axis axis;

        public bool addRandomOffset;
        float offset;
        float startAxisPos;

        protected void Start() {
            startAxisPos = transform.localPosition[(int)axis];
            if ( addRandomOffset )
                offset = Random.value * Mathf.PI * 2f;
        }

        protected void Update() {
            Vector3 pos = transform.localPosition;
            pos[(int)axis] = startAxisPos + Mathf.Sin( Time.time * 2f * Mathf.PI / period + offset ) * amplitude;
            transform.localPosition = pos;
        }

        public enum Axis {
            X,
            Y,
            Z
        }
    }
}