﻿/* Follow.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Follow : MonoBehaviour {
    public Transform target;
    public bool followX = true, followY = true, followZ = true;
    public Vector3 offset;

    protected void Update() {
        if ( target ) {
            transform.position = new Vector3(
                followX ? target.position.x + offset.x: transform.position.x,
                followY ? target.position.y + offset.y: transform.position.y,
                followZ ? target.position.z + offset.z: transform.position.z
                );
        }
    }
}
