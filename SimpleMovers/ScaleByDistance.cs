﻿/* ScaleByDistance.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Scale this transform by it's distance to another transform
/// </summary>
public class ScaleByDistance : MonoBehaviour {
    public float minScale, maxScale;
    public float minDistance, maxDistance;

    public Transform target;

    void Update() {
        if ( !target )
            return;

        float sqDistToTarget = ( target.position - transform.position ).sqrMagnitude;
        float scaleAmount = Mathf.InverseLerp( minDistance * minDistance, maxDistance * maxDistance, sqDistToTarget );
        transform.localScale = Vector3.one * Mathf.SmoothStep( minScale, maxScale, scaleAmount );
    }
}
