/* MathUtils.cs
 * Copyright Eddie Cameron & Grasshopper 2014
 * Open-sourced under the MIT licence
 * 
 * EXCEPT CombinatoricsUtilities Copyright Alex Reguiero 2010 (MIT licenced)
 * ----------------------------
 * A small collection of math utilities
 */
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityRandom = UnityEngine.Random;

namespace MathUtils
{
    /// <summary>
    /// Angle & trig related utilities
    /// </summary>
    public static class Geometry
    {
        /// <summary>
        /// Fix any angle to its equivilent between 0 &amp; 360
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float NormaliseAngleDegrees( float angle )
        {
            while ( angle < 0 )
                angle += 360f;
            while ( angle >= 360f )
                angle -= 360f;
            
            return angle;
        }

        /// <summary>
        /// Fix any radian angle to its equivilent between 0 &amp; 2.PI
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float NormaliseAngleRadians( float angle )
        {
            var twoPi = Mathf.PI * 2;
            while ( angle < 0 )
                angle += twoPi;
            while ( angle >= twoPi )
                angle -= twoPi;
            
            return angle;
        }

        public static float GetClockwiseAngle( Vector2 fromVector, Vector2 toVector ) {
            float angle = Vector2.Angle( fromVector, toVector );

            Vector3 cross = Vector3.Cross( fromVector, toVector );
            float crossDotZ = Vector3.Dot( cross, Vector3.forward );
            if ( crossDotZ < 0 )
                angle = 360f - angle; 

            return angle;
        }

        /// <summary>
        /// Intersects the ray with plane defined by <paramref name="planePoint"/> and <paramref name="planeNormal"/>
        /// planeNormal defaults to world up
        /// </summary>
        /// <returns>Whether there was an intersection.</returns>
        /// <param name="hitPoint">Hitpoint to set</param>
        /// <param name="ray">Ray.</param>
        /// <param name="planePoint">Plane point.</param>
        /// <param name="planeNormal">Plane normal.</param>
        public static bool IntersectRayWithPlane( out Vector3 hitPoint, Ray ray, Vector3 planePoint, Vector3 planeNormal = default( Vector3 ) )
        {
            if ( planeNormal == default( Vector3 ) )
                planeNormal = Vector3.up;
            float rP = Vector3.Dot( ray.direction, planeNormal );
            if ( Mathf.Abs( rP - 0 ) < Mathf.Epsilon )
            {
                hitPoint = Vector3.zero;
                return false;
            }
            hitPoint = ray.GetPoint( Vector3.Dot( planePoint - ray.origin, planeNormal ) / rP );
            return true;
        }

        //http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
        public static bool IntersectLines( Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2 ) {
            // Find the four orientations needed for general and
            // special cases
            int o1 = GetTriOrientation( a1, a2, b1 );
            int o2 = GetTriOrientation( a1, a2, b2 );
            int o3 = GetTriOrientation( b1, b2, a1 );
            int o4 = GetTriOrientation( b1, b2, a2 );

            // General case
            if ( o1 != o2 && o3 != o4 )
                return true;

            // Special Cases
            // p1, q1 and p2 are colinear and p2 lies on segment p1q1
            if ( o1 == 0 && IsOnSegment( a1, b1, a2 ) ) return true;

            // p1, q1 and p2 are colinear and q2 lies on segment p1q1
            if ( o2 == 0 && IsOnSegment( a1, b2, a2 ) ) return true;

            // p2, q2 and p1 are colinear and p1 lies on segment p2q2
            if ( o3 == 0 && IsOnSegment( b1, a1, b2 ) ) return true;

            // p2, q2 and q1 are colinear and q1 lies on segment p2q2
            if ( o4 == 0 && IsOnSegment( b1, a2, b2 ) ) return true;

            return false; // Doesn't fall in any of the above cases
        }

        // To find orientation of ordered triplet (p, q, r).
        // The function returns following values
        // 0 --> p, q and r are colinear
        // 1 --> Clockwise
        // 2 --> Counterclockwise
        static int GetTriOrientation( Vector2 p, Vector2 q, Vector2 r ) {
            // See http://www.geeksforgeeks.org/orientation-3-ordered-points/
            // for details of below formula.
            float val = ( q.y - p.y ) * ( r.x - q.x ) -
                            ( q.x - p.x ) * ( r.y - q.y );

            if (Math.Abs( val ) < float.Epsilon ) return 0;  // colinear

            return ( val > 0 ) ? 1 : 2; // clock or counterclock wise
        }

        // Given three colinear points p, q, r, the function checks if
        // point q lies on line segment 'pr'
        static bool IsOnSegment( Vector2 p, Vector2 q, Vector2 r ) {
            if ( q.x <= Math.Max( p.x, r.x ) && q.x >= Math.Max( p.x, r.x ) &&
                q.y <= Math.Max( p.y, r.y ) && q.y >= Math.Max( p.y, r.y ) )
                return true;

            return false;
        }

        public static Vector3 RandomOnXZCircle() {
            float angle = UnityRandom.value * Mathf.PI * 2;
            return new Vector3( Mathf.Cos( angle ), 0, Mathf.Sin( angle ) );
        }

        public static Vector3 RandomPointWithin( this Bounds bounds ) {
            return bounds.min + new Vector3(
                bounds.size.x * UnityRandom.value,
                bounds.size.y * UnityRandom.value,
                bounds.size.z * UnityRandom.value
                );
        }

        /// <summary>
        /// Shrink a polygon defined by <paramref name="bounds"/> by <paramref name="shrinkDistance"/>
        /// If <paramref name="keepSimple"/> is true, will remove any points that end up outside polygon
        /// </summary>        
        /// <param name="bounds"></param>
        /// <param name="shrinkDistance"></param>
        /// <param name="keepSimple"></param>
        /// <returns></returns>
        public static Vector2[] ShrinkPolygon( IList<Vector2> bounds, float shrinkDistance, bool keepSimple = true ) {
            var newBounds = new List<Vector2>();
            for ( int i = 0; i < bounds.Count; i++ ) {
                int lastIndex = i - 1;
                if ( lastIndex < 0 )
                    lastIndex = bounds.Count - 1;
                int nextIndex = ( i + 1 ) % bounds.Count;

                // find direction to move point by
                Vector2 avgDir = ( bounds[lastIndex] - bounds[i] ).normalized + ( bounds[nextIndex] - bounds[i] ).normalized;
                avgDir *= .5f;

                if ( !ContainsPoint( bounds, bounds[i] + avgDir * shrinkDistance * .1f ) ) // TODO find method that still works w/ very small polygons
                    avgDir = -avgDir;

                var newPoint = bounds[i] + avgDir * shrinkDistance;
                if ( !keepSimple || ContainsPoint( bounds, newPoint ) )
                    newBounds.Add( newPoint );
            }
            return newBounds.ToArray();
        }

        /// <summary>
        /// Return centre/average of given points
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static Vector2 GetAverageOfPoints( IEnumerable<Vector2> points ) {
            Vector2 total = Vector2.zero;
            int count = 0;
            foreach ( var point in points ) {
                total += point;
                count++;
            }
            return total / count;
        }

        /// <summary>
        /// Does the polygon defined by <paramref name="bounds"/> contain point <paramref name="p"/>
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool ContainsPoint( IList<Vector2> bounds, Vector2 p ) {
            var j = bounds.Count - 1;
            var inside = false;
            for ( int i = 0; i < bounds.Count; j = i++ ) {
                if ( ( ( bounds[i].y <= p.y && p.y < bounds[j].y ) || ( bounds[j].y <= p.y && p.y < bounds[i].y ) ) &&
                    ( p.x < ( bounds[j].x - bounds[i].x ) * ( p.y - bounds[i].y ) / ( bounds[j].y - bounds[i].y ) + bounds[i].x ) )
                    inside = !inside;
            }
            return inside;
        }

        public static Vector2 GetClosestPointOnEdge( Vector2 fromPoint, Vector2 a, Vector2 b ) {
            float abSquared = ( a - b ).sqrMagnitude;
            if ( Mathf.Approximately( abSquared, 0 ) ) return a; // points are together

            //project a to fromPoint onto ab
            float t = Mathf.Clamp01( Vector2.Dot( fromPoint - a, b - a ) / abSquared );
            return a + t * ( a - b );
        }

        public static float GetDistanceToEdge( Vector2 fromPoint, Vector2 a, Vector2 b ) {
            // http://mathworld.wolfram.com/Point-LineDistance2-Dimensional.html
            return Mathf.Abs( ( b.x - a.x ) * ( b.y - fromPoint.y ) - ( a.x - fromPoint.x ) * ( b.y - a.y ) ) / ( b - a ).magnitude;
        }

        /// <summary>
        /// Convert Vector2(x, y) into Vector3(x, 0, y)
        /// (Implicit conversion turns it into Vector3(x, y, 0)
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Vector3 AsXZVector3( this Vector2 a ) {
            return new Vector3( a.x, 0, a.y );
        }

        /// <summary>
        /// Convert Vector3(x, y, z) into Vector2(x, z)
        /// (Implicit conversion turns it into Vector2(x, y)
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Vector2 AsXZVector2( this Vector3 a ) {
            return new Vector2( a.x, a.z );
        }

        public static Vector2 Rotate( this Vector2 v, float radians ) {
            float sin = Mathf.Sin( radians );
            float cos = Mathf.Cos( radians );

            float tx = v.x;
            float ty = v.y;
            v.x = ( cos * tx ) - ( sin * ty );
            v.y = ( sin * tx ) + ( cos * ty );

            return v;        }
    }

    public class Hull
    {
        struct Line
        {
            public int a;
            public int b;

            public Line( int a, int b )
            {
                this.a = a;
                this.b = b;
            }
        }

        /// <summary>
        /// Make a convex hull from given vertices
        /// </summary>
        /// <param name="mesh">Mesh.</param>
        /// <param name="vertices">Vertices.</param>
        /// Uses 'Gift-wrap' algorithm
        public static void MakeConvexHull( Mesh mesh, Vector3[] vertices )
        {
            if ( vertices.Length < 3 )
            {
                DebugExtras.LogWarning( "Can't find hull of less than 3 points" );
                return;
            }

            // DEBUG
            /*
            var verticesString = new System.Text.StringBuilder( "Making Hull from vertices:" );
            for ( int i = 0; i < vertices.Length; i++ )
                verticesString.AppendLine( i + " " + vertices[i] * 10 ); // * 10 so unity shows dp
            DebugExtras.Log( verticesString );
            */

            // create start line AB (in 2D proj) where all other points are to the right
            // unity has cockwise winding, so reverse to some implementations

            // find lowest point as A
            Vector3 a = vertices[0];
            int aIndex = 0;
            for ( int i = 1; i < vertices.Length; i++ )
            {
                if ( vertices[i].y < a.y )
                {
                    a = vertices[i];
                    aIndex = i;
                }
            }

            // find B where all other points are to right of AB
            int bIndex = aIndex == 0 ? 1 : 0;
            Vector3 b = vertices[bIndex];
            for ( int i = 1; i < vertices.Length; i++ )
            {
                if ( i != aIndex && i != bIndex )
                {
                    if ( GetPointPosition( a, b, vertices[i] ) == PointPosition.Left )
                    {
                        // move b to new leftmost vertex
                        bIndex = i;
                        b = vertices[i];
                    }
                }
            }

            var triangles = new List<int>();
            // while there are still 'free' edges in queue, look for hull triangle on each, and add the new free dges to queue
            var edgesToTriangulate = new Queue<Line>();
            var edgesTriangulated = new HashSet<Line>();

            edgesToTriangulate.Enqueue( new Line( aIndex, bIndex ) );
            while ( edgesToTriangulate.Count > 0 )
            {
                var nextLine = edgesToTriangulate.Dequeue();
                if ( edgesTriangulated.Contains( nextLine ) )
                    continue;

                //DebugExtras.Log( "Finding triangle from edge: " + nextLine.a + ", " + nextLine.b );
                int newC = FindGiftWrapTriangle( nextLine.a, nextLine.b, vertices );
                triangles.Add( nextLine.a );
                triangles.Add( nextLine.b );
                triangles.Add( newC );

                /*
                DebugExtras.Log( "Found hull triangle: " + nextLine.a + ", " + nextLine.b + ", " + newC );
                UnityEngine.Debug.DrawLine( vertices[nextLine.a], vertices[nextLine.b] );
                UnityEngine.Debug.DrawLine( vertices[nextLine.b], vertices[newC] );
                UnityEngine.Debug.DrawLine( vertices[newC], vertices[nextLine.a] );
                */

                edgesTriangulated.Add( nextLine );
                edgesTriangulated.Add( new Line( nextLine.b, newC ) );
                edgesTriangulated.Add( new Line( newC, nextLine.a ) );

                Line ac = new Line( nextLine.a, newC );
                edgesToTriangulate.Enqueue( ac );
                Line cb = new Line( newC, nextLine.b );
                edgesToTriangulate.Enqueue( cb );
                //DebugExtras.Log( "Edges to find " + edgesToTriangulate.Count );
            }

            // TODO remove redundant vertices
            mesh.vertices = vertices;
            mesh.triangles = triangles.ToArray();
        }

        /// <summary>
        /// find C where all other points are to right (away from normal) of ABC
        /// </summary>
        /// <returns>Index of point C.</returns>
        /// <param name="vertices">Vertices.</param>
        static int FindGiftWrapTriangle( int a, int b, Vector3[] vertices )
        {
            int c = -1; // needs to start at first available vertex

            Vector3 aVector = vertices[a];
            Vector3 bVector = vertices[b];
            Vector3 cVector = Vector3.zero;

            for ( int i = 0; i < vertices.Length; i++ )
            {
                if ( i != a && i != b )
                {
                    if ( c == -1 ) // put c at first unused vertex
                    {
                        c = i;
                        cVector = vertices[i];
                    }
                    else if ( i != c )
                    {
                        switch( GetPointPosition( aVector, bVector, cVector, vertices[i] ) )
                        {
                        case PointPosition.Left: // reversed because unity uses CW winding (unity in-front means CCW behind)
                            //DebugExtras.Log( i + " in front of " + a + " " + b + " " + c );
                            // move C to new 'leftmost' vertex
                            c = i;
                            cVector = vertices[i];
                            break;
                        case PointPosition.Colinear:
                            //DebugExtras.Log( i + " colinear with " + a + " " + b + " " + c );
                            break;
                        }
                    }
                }
            }

            return c;
        }

        /// <summary>
        /// Whether p is to left of line AB
        /// </summary>
        /// <returns><c>true</c>, if to left was pointed, <c>false</c> otherwise.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="p">P.</param>
        public static PointPosition GetPointPosition( Vector2 a, Vector2 b, Vector2 p )
        {
            float det = a.x * ( b.y - p.y ) + b.x * ( p.y - a.y ) + p.x * ( a.y - b.y );
            if ( Mathf.Abs( det ) < ColinearThreshold )
                return PointPosition.Colinear;
            if ( det > 0 )
                return PointPosition.Left; // positive if anticlockwise ( c to left of AB)
            return PointPosition.Right;
        }

        /// <summary>
        /// Whether P is to left (in direction of CCW normal),to right, or coplanar of ABC
        /// </summary>
        /// <returns>The point position.</returns>
        public static PointPosition GetPointPosition( Vector3 a, Vector3 b, Vector3 c, Vector3 p )
        {
            // det of 3x3 made up of AB, AC, AP
            Vector3 ab = b - a;
            Vector3 ac = c - a;
            Vector3 ap = p - a;

            // pos if normal of ABC points towards P
            float det = ( ab.x * ac.y * ap.z + ab.y * ac.z * ap.x + ab.z * ac.x * ap.y ) - ( ab.z * ac.y * ap.x + ab.y * ac.x * ap.z + ab.x * ac.z * ap.y );
            if ( Mathf.Abs( det ) < ColinearThreshold )
                return PointPosition.Colinear;
            if ( det > 0 )
                return PointPosition.Left;
            return PointPosition.Right;
        }

        const float ColinearThreshold = 0.001f; // min determinant for points to be considered colinear

        public enum PointPosition
        {
            Left,
            Right,
            Colinear
        }
    }

    /// <summary>
    /// String formatting utilities
    /// </summary>
    public static class Formatting
    {
        /// <summary>
        /// Format any float to given number of sig digits
        /// </summary>
        /// <param name="toRound"></param>
        /// <param name="digits"></param>
        /// <returns></returns>
        public static string FormatToSignificantDigits( float toRound, int digits )
        {
            if ( Mathf.Abs( toRound - 0 ) < Mathf.Epsilon )
                return toRound.ToString();

            // 'normalise' to put all digits after decimal place, round, then format
            // based on http://stackoverflow.com/questions/374316/round-a-double-to-x-significant-figures/374470#374470
            int leftSideNumbers = Mathf.FloorToInt( Mathf.Log10( Mathf.Abs( toRound ) ) ) + 1;
            decimal scale = (decimal)Mathf.Pow( 10, leftSideNumbers );
            return ((double)( scale * Math.Round( (decimal)toRound / scale, digits, MidpointRounding.AwayFromZero ) )).ToString();
        }

        public static string GetLogString<T>( this ICollection<T> collection ) {
            StringBuilder sb = new StringBuilder();
            sb.Append('[' );
            bool first = true;
            foreach ( var entry in collection ) {
                if ( first )
                    first = false;
                else
                    sb.Append( ", " );
                sb.Append( entry );
            }
            sb.Append( ']' );
            return sb.ToString();
        }
    }

    /// <summary>
    /// Combinations/permutations helpers
    /// </summary>
    public static class CombinatoricsUtilities
    {
        // Copyright (c) 2010 Alex Regueiro
        // Licensed under MIT license, available at <http://www.opensource.org/licenses/mit-license.php>.
        // Published originally at <http://blog.noldorin.com/2010/05/combinatorics-in-csharp/>.
        // Version 1.0, released 22nd May 2010.

        // Error messages
        private const string errorMessageValueLessThanZero = "Value must be greater than zero, if specified.";
        private const string errorMessagesIndicesListInvalidSize = "List of indices must have same size as list of elements.";

        /// <summary>
        /// Gets all permutations (of a given size) of a given list, either with or without reptitions.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the list.</typeparam>
        /// <param name="list">The list of which to get permutations.</param>
        /// <param name="action">The action to perform on each permutation of the list.</param>
        /// <param name="resultSize">The number of elements in each resulting permutation; or <see langword="null"/> to get
        /// premutations of the same size as <paramref name="list"/>.</param>
        /// <param name="withRepetition"><see langword="true"/> to get permutations with reptition of elements;
        /// <see langword="false"/> to get permutations without reptition of elements.</param>
        /// <exception cref="ArgumentNullException"><paramref name="list"/> is <see langword="null"/>. -or-
        /// <paramref name="action"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="resultSize"/> is less than zero.</exception>
        /// <remarks>
        /// The algorithm performs permutations in-place. <paramref name="list"/> is however not changed.
        /// </remarks>
        public static void GetPermutations<T>(this IList<T> list, Action<IList<T>> action, int? resultSize = null,
                                              bool withRepetition = false)
        {
            if (list == null)
                throw new ArgumentNullException("list");
            if (action == null)
                throw new ArgumentNullException("action");
            if (resultSize.HasValue && resultSize.Value <= 0)
                throw new ArgumentException(errorMessageValueLessThanZero, "resultSize");

            var result = new T[resultSize.HasValue ? resultSize.Value : list.Count];
            var indices = new int[result.Length];
            for (int i = 0; i < indices.Length; i++)
                indices[i] = withRepetition ? -1 : i - 1;

            int curIndex = 0;
            while (curIndex != -1)
            {
                indices[curIndex]++;
                if (indices[curIndex] == list.Count)
                {
                    indices[curIndex] = withRepetition ? -1 : curIndex - 1;
                    curIndex--;
                }
                else
                {
                    result[curIndex] = list[indices[curIndex]];
                    if (curIndex < indices.Length - 1)
                        curIndex++;
                    else
                        action(result);
                }
            }
        }

        /// <summary>
        /// Gets all combinations (of a given size) of a given list, either with or without reptitions.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the list.</typeparam>
        /// <param name="list">The list of which to get combinations.</param>
        /// <param name="action">The action to perform on each combination of the list.</param>
        /// <param name="resultSize">The number of elements in each resulting combination; or <see langword="null"/> to get
        /// premutations of the same size as <paramref name="list"/>.</param>
        /// <param name="withRepetition"><see langword="true"/> to get combinations with reptition of elements;
        /// <see langword="false"/> to get combinations without reptition of elements.</param>
        /// <exception cref="ArgumentNullException"><paramref name="list"/> is <see langword="null"/>. -or-
        /// <paramref name="action"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="resultSize"/> is less than zero.</exception>
        /// <remarks>
        /// The algorithm performs combinations in-place. <paramref name="list"/> is however not changed.
        /// </remarks>
        public static void GetCombinations<T>(this IList<T> list, Action<IList<T>> action, int? resultSize = null,
                                              bool withRepetition = false)
        {
            if (list == null)
                throw new ArgumentNullException("list");
            if (action == null)
                throw new ArgumentNullException("action");
            if (resultSize.HasValue && resultSize.Value <= 0)
                throw new ArgumentException(errorMessageValueLessThanZero, "resultSize");

            var result = new T[resultSize.HasValue ? resultSize.Value : list.Count];
            var indices = new int[result.Length];
            for (int i = 0; i < indices.Length; i++)
                indices[i] = withRepetition ? -1 : indices.Length - i - 2;

            int curIndex = 0;
            while (curIndex != -1)
            {
                indices[curIndex]++;
                if (indices[curIndex] == (curIndex == 0 ? list.Count : indices[curIndex - 1] + (withRepetition ? 1 : 0)))
                {
                    indices[curIndex] = withRepetition ? -1 : indices.Length - curIndex - 2;
                    curIndex--;
                }
                else
                {
                    result[curIndex] = list[indices[curIndex]];
                    if (curIndex < indices.Length - 1)
                        curIndex++;
                    else
                        action(result);
                }
            }
        }

        /// <summary>
        /// Gets a specific permutation of a given list.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the list.</typeparam>
        /// <param name="list">The list to permute.</param>
        /// <param name="indices">The indices of the elements in the original list at each index in the permuted list.
        /// </param>
        /// <returns>The specified permutation of the given list.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="list"/> is <see langword="null"/>. -or-
        /// <paramref name="indices"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException"><paramref name="indices"/> does not have the same size as
        /// <paramref name="list"/>.</exception>
        public static IList<T> Permute<T>(this IList<T> list, IList<int> indices)
        {
            if (list == null)
                throw new ArgumentNullException("list");
            if (indices == null)
                throw new ArgumentNullException("indices");
            if (list.Count != indices.Count)
                throw new ArgumentException(errorMessagesIndicesListInvalidSize, "indices");

            var result = new T[list.Count];
            for (int i = 0; i < result.Length; i++)
                result[i] = list[indices[i]];
            return result;
        }
    }


    public static class Misc {
        public static T RandomEnum<T>() {
            T[] values = (T[])Enum.GetValues( typeof( T ) );
            return values[UnityRandom.Range( 0, values.Length )];
        }
    }
}