﻿/* MinMaxRangeDrawer.cs
 * by Eddie Cameron - For the public domain
 * -----------------------------------------------------------
 * --- EDITOR SCRIPT : Place in a subfolder named 'Editor' ---
 * -----------------------------------------------------------
 * Renders a MinMaxRange field with a MinMaxRangeAttribute as a slider in the inspector
 * Can slide either end of the slider to set ends of range
 * Can slide whole slider to move whole range
 * Can enter exact range values into the From: and To: inspector fields
 * 
 */

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Show a minmaxrange slider in inspector. Use with <see cref="MinMaxRangeFloat"/> & <see cref="MinMaxRangeInt"/>
/// </summary>
[CustomPropertyDrawer( typeof( MinMaxRangeAttribute ) )]
[CustomPropertyDrawer( typeof( MinMaxRangeFloat ) )]
[CustomPropertyDrawer( typeof( MinMaxRangeInt ) )]
public class MinMaxRangeDrawer : PropertyDrawer
{
    float rangeMin, rangeMax;

    public override float GetPropertyHeight( SerializedProperty property, GUIContent label )
    {
        return base.GetPropertyHeight( property, label ) + 16;
    }

    // Draw the property inside the given rect
    public override void OnGUI( Rect position, SerializedProperty property, GUIContent label ) {
        var propType = property.type;
        // Now draw the property as a Slider or an IntSlider based on whether it's a float or integer.
        if ( propType != "MinMaxRangeFloat" && propType != "MinMaxRangeInt" )
            Debug.LogWarning( "Use only with MinMaxRange types" );

        var minValue = property.FindPropertyRelative( "rangeStart" );
        var maxValue = property.FindPropertyRelative( "rangeEnd" );

        bool useInt = propType == "MinMaxRangeInt";
        float newMin = useInt ? minValue.intValue : minValue.floatValue;
        float newMax = useInt ? maxValue.intValue : maxValue.floatValue;

        var range = attribute as MinMaxRangeAttribute;
        if ( range != null ) {
            rangeMin = range.minLimit;
            rangeMax = range.maxLimit;
        }
        else {
            rangeMin = Mathf.Min( rangeMin, newMin );
            rangeMax = Mathf.Max( rangeMax, newMax );
        }

        var xDivision = position.width * 0.33f;
        var yDivision = position.height * 0.5f;
        EditorGUI.LabelField( new Rect( position.x, position.y, xDivision, yDivision )
                             , label );

        float rangeWidth = 28f;
        if ( range != null ) {
            // use range from code
            string formatString = useInt ? "f0" : "0.##";
            EditorGUI.LabelField( new Rect( position.x, position.y + yDivision, rangeWidth, yDivision )
                             , rangeMin.ToString( formatString ) );
            EditorGUI.LabelField( new Rect( position.x + position.width - rangeWidth, position.y + yDivision, rangeWidth, yDivision )
                                 , rangeMax.ToString( formatString ) );
        }
        else {
            // use range fields in inspector
            if ( useInt ) {
                rangeMin = EditorGUI.DelayedIntField( new Rect( position.x, position.y + yDivision, rangeWidth, yDivision )
                                 , (int)rangeMin );
                rangeMax = EditorGUI.DelayedIntField( new Rect( position.x + position.width - rangeWidth, position.y + yDivision, rangeWidth, yDivision )
                                     , (int)rangeMax );
            }
            else {
                rangeMin = EditorGUI.DelayedFloatField( new Rect( position.x, position.y + yDivision, rangeWidth, yDivision )
                                 , rangeMin );
                rangeMax = EditorGUI.DelayedFloatField( new Rect( position.x + position.width - rangeWidth, position.y + yDivision, rangeWidth, yDivision )
                                     , rangeMax );
            }
        }

        // draw slider
        EditorGUI.MinMaxSlider( new Rect( position.x + rangeWidth, position.y + yDivision, position.width - 2 * rangeWidth, yDivision )
                               , ref newMin, ref newMax, rangeMin, rangeMax );

        // from field
        EditorGUI.LabelField( new Rect( position.x + xDivision, position.y, xDivision, yDivision )
                             , "From: " );
        if ( useInt ) {
            newMin = Mathf.Clamp( EditorGUI.DelayedIntField( new Rect( position.x + xDivision + 30, position.y, xDivision - 30, yDivision )
                                    , (int)newMin )
                                 , rangeMin, rangeMax );
        }
        else {
            newMin = Mathf.Clamp( EditorGUI.DelayedFloatField( new Rect( position.x + xDivision + 30, position.y, xDivision - 30, yDivision )
                                , newMin )
                             , rangeMin, rangeMax );
        }
        if ( newMin > newMax )
            newMax = newMin;

        // to field
        if ( useInt ) {
            EditorGUI.LabelField( new Rect( position.x + xDivision * 2f, position.y, xDivision, yDivision )
                                 , "To: " );
            newMax = Mathf.Clamp( EditorGUI.DelayedIntField( new Rect( position.x + xDivision * 2f + 24, position.y, xDivision - 24, yDivision )
                                    , (int)newMax )
                                 , rangeMin, rangeMax );

        }
        else {
            EditorGUI.LabelField( new Rect( position.x + xDivision * 2f, position.y, xDivision, yDivision )
                                 , "To: " );
            newMax = Mathf.Clamp( EditorGUI.DelayedFloatField( new Rect( position.x + xDivision * 2f + 24, position.y, xDivision - 24, yDivision )
                                    , newMax )
                                 , rangeMin, rangeMax );
        }
        if ( newMax < newMin )
            newMin = newMax;

        if ( useInt ) {
            minValue.intValue = (int)newMin;
            maxValue.intValue = (int)newMax;
        }
        else {
            minValue.floatValue = newMin;
            maxValue.floatValue = newMax;
        }
    }
}
