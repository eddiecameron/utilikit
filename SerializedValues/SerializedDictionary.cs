﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializedDictionary<TKey, TValue> : SerializedValue, IDictionary<TKey, TValue> {

    [Serializable]
    class JsonDictionary : JsonDictionary<TKey, TValue> { }
    JsonDictionary _dict;

    public SerializedDictionary( string cacheID ) : base( cacheID ) {
    }

    protected override bool TryParseValueFromCacheString( string cacheString ) {
        _dict = JsonUtility.FromJson<JsonDictionary>( cacheString );
        return _dict != null;
    }

    protected override void SetValueToDefault() {
        _dict = new JsonDictionary();
    }

    protected override string GetCacheStringFromValue() {
        if ( _dict == null )
            SetValueToDefault();
        return JsonUtility.ToJson( _dict );
    }

    public ICollection<TKey> Keys {
        get {
            LoadFromDisk();
            return _dict.dict.Keys;
        }
    }

    public ICollection<TValue> Values {
        get {
            LoadFromDisk();
            return _dict.dict.Values;
        }
    }

    public int Count {
        get {
            LoadFromDisk();
            return _dict.dict.Count;
        }
    }

    public bool IsReadOnly => false;

    public TValue this[TKey key] {
        get {
            LoadFromDisk();
            return _dict.dict[key];
        }
        set {
            LoadFromDisk();
            _dict.dict[key] = value;
            SaveToDisk();
        }
    }

    public bool ContainsKey( TKey key ) {
        LoadFromDisk();
        return _dict.dict.ContainsKey( key );
    }

    public void Add( TKey key, TValue value ) {
        this[key] = value;
    }

    public bool Remove( TKey key ) {
        LoadFromDisk();
        bool didRemove = _dict.dict.Remove( key );
        SaveToDisk();
        return didRemove;
    }

    public bool TryGetValue( TKey key, out TValue value ) {
        LoadFromDisk();
        return _dict.dict.TryGetValue( key, out value );
    }

    public void Add( KeyValuePair<TKey, TValue> item ) {
        this[item.Key] = item.Value;
    }

    public void Clear() {
        LoadFromDisk();
        _dict.dict.Clear();
        SaveToDisk();
    }

    public bool Contains( KeyValuePair<TKey, TValue> item ) {
        TValue value;
        return TryGetValue( item.Key, out value ) && value.Equals( item.Value );
    }

    public void CopyTo( KeyValuePair<TKey, TValue>[] array, int arrayIndex ) {
        LoadFromDisk();
        foreach ( var item in _dict.dict ) {
            array[arrayIndex] = item;
            arrayIndex++;
        }
    }

    public bool Remove( KeyValuePair<TKey, TValue> item ) {
        return Contains( item ) && Remove( item.Key );
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
        LoadFromDisk();
        return _dict.dict.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        LoadFromDisk();
        return _dict.dict.GetEnumerator();
    }

    [Serializable]
    private abstract class JsonDictionary<TBaseKey, TBaseValue> : ISerializationCallbackReceiver {

        [SerializeField]
        private List<TBaseKey> _keys = new List<TBaseKey>();
        [SerializeField]
        private List<TBaseValue> _values = new List<TBaseValue>();

        public Dictionary<TBaseKey, TBaseValue> dict = new Dictionary<TBaseKey, TBaseValue>();

        void ISerializationCallbackReceiver.OnBeforeSerialize() {
            _keys.Clear();
            _values.Clear();
            foreach ( var kv in dict ) {
                _keys.Add( kv.Key );
                _values.Add( kv.Value );
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize() {
            dict.Clear();

            if ( _keys.Count != _values.Count )
                throw new SerializedValueException( "Serialized key and value length mismatch" );

            for ( int i = 0; i < _keys.Count; i++ ) {
                bool isUniqueKey = dict.TryAdd( _keys[i], _values[i] );
                if ( !isUniqueKey )
                    throw new SerializedValueException( "Key serialized multiple times" );
            }
        }
    }
}

