﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SerializableInt {
    [SerializeField]
    private int value;
    public int Value { get { return value; } }

    public SerializableInt( int value ) {
        this.value = value;
    }

    public static implicit operator int( SerializableInt s ) {
        return s.value;
    }

    public static implicit operator SerializableInt( int i ) {
        return new SerializableInt( i );
    }
}
