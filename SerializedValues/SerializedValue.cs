﻿/* SerializedValue.cs
 * Copyright Eddie Cameron 2017
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;
using System.IO;

/// <summary>
/// A value that auto-serializes to disk
/// </summary>
public abstract class SerializedValue {

    protected readonly string _cacheID;

    protected bool HaveReadFromDisk { get; private set; }

    bool _haveSetValue_;
    public bool HaveSetValue {
        get {
            if ( !_haveSetValue_ && !HaveReadFromDisk ) {
                LoadFromDisk();
            }
            return _haveSetValue_;
        }
    }

    public SerializedValue( string cacheID ) {
        _cacheID = cacheID;
    }

    public void LoadFromDisk() {
        if ( HaveReadFromDisk )
            return;

        string cacheString = ReadString( _cacheID );

        bool didParse = false;
        if ( !string.IsNullOrEmpty( cacheString ) ) {
            // try parse json
            try {
                didParse = TryParseValueFromCacheString( cacheString );
            }
            catch ( Exception e ) {
                SetValueToDefault();
                throw new SerializedValueException( "Unhandled failure to parse cached string into correct type", e );
            }
        }

        if ( !didParse ) {
            SetValueToDefault();
        }

        HaveReadFromDisk = true;
        _haveSetValue_ = didParse;
    }

    public void SaveToDisk() {
        string cacheString = GetCacheStringFromValue();

        SaveString( _cacheID, cacheString );
        HaveReadFromDisk = true;
        _haveSetValue_ = true;
    }

    /// <summary>
    /// Process the run-time value from given cached string
    /// </summary>
    /// <returns><c>true</c>, if value from cache string was processed, <c>false</c> otherwise.</returns>
    /// <param name="cacheString">Cache string.</param>
    protected abstract bool TryParseValueFromCacheString( string cacheString );

    /// <summary>
    /// Set value to it's default state (after failing to read from disk)
    /// </summary>
    protected abstract void SetValueToDefault();

    /// <summary>
    /// Get a cacheable string from the run-time value
    /// </summary>
    /// <returns>The cache string from value.</returns>
    protected abstract string GetCacheStringFromValue();

    protected virtual string ReadString( string key ) {
        return PlayerPrefs.GetString( key, "" );
    }

    protected virtual void SaveString( string key, string value ) {
        PlayerPrefs.SetString( key, value );
    }

    public class SerializedValueException : Exception {
        public SerializedValueException( string message ) : base( message ) { }
        public SerializedValueException( string message, Exception innerException ) : base( message, innerException ) { }
    }
}

public class SerializedValue<T> : SerializedValue {
    private T _value;

    public SerializedValue( string cacheID ) : base( cacheID ) { }

    public T Value
    {
        get {
            if ( !HaveReadFromDisk ) {
                LoadFromDisk();
            }
            return _value;
        }

        set {
            _value = value;
            SaveToDisk();
        }
    }

    protected override bool TryParseValueFromCacheString( string cacheString ) {
        _value = JsonUtility.FromJson<T>( cacheString );
        return true;
    }

    protected override void SetValueToDefault() {
        _value = default( T );
    }

    protected override string GetCacheStringFromValue() {
        string cacheString = JsonUtility.ToJson( _value );

        if ( cacheString == "{}" ) {
            throw new SerializedValueException( "Failed To Create Serialized JSON string For Type : " + typeof( T ) );
        }
        return cacheString;
    }
}
