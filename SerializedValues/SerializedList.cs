﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializedList<T> : SerializedValue, IList<T> {

    [Serializable]
    class JsonList : JsonList<T> { }
    JsonList _list;

    public SerializedList( string cacheID ) : base( cacheID ) {
    }

    protected override string GetCacheStringFromValue() {
        if ( _list == null )
            SetValueToDefault();
        return JsonUtility.ToJson( _list );
    }

    protected override void SetValueToDefault() {
        _list = new JsonList();
    }

    protected override bool TryParseValueFromCacheString( string cacheString ) {
        _list = JsonUtility.FromJson<JsonList>( cacheString );
        return _list != null;
    }

    public T this[int index]
    {
        get {
            LoadFromDisk();
            return _list.list[index];
        }
        set {
            LoadFromDisk();
            _list.list[index] = value;
            SaveToDisk();
        }
    }

    public int Count {
        get {
            LoadFromDisk();
            return _list.list.Count;
        }
    }

    public bool IsReadOnly => false;

    public void Add( T item ) {
        LoadFromDisk();
        _list.list.Add( item );
        SaveToDisk();
    }

    public void Clear() {
        LoadFromDisk();
        _list.list.Clear();
        SaveToDisk();
    }

    public bool Contains( T item ) {
        LoadFromDisk();
        return _list.list.Contains( item );
    }

    public void CopyTo( T[] array, int arrayIndex ) {
        LoadFromDisk();
        _list.list.CopyTo( array, arrayIndex );
    }

    public IEnumerator<T> GetEnumerator() {
        LoadFromDisk();
        return _list.list.GetEnumerator();
    }

    public int IndexOf( T item ) {
        LoadFromDisk();
        return _list.list.IndexOf( item );
    }

    public void Insert( int index, T item ) {
        LoadFromDisk();
        _list.list.Insert( index, item );
        SaveToDisk();
    }

    public bool Remove( T item ) {
        LoadFromDisk();
        bool didRemove = _list.list.Remove( item );
        SaveToDisk();
        return didRemove;
    }

    public void RemoveAt( int index ) {
        LoadFromDisk();
        _list.list.RemoveAt( index );
        SaveToDisk();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        LoadFromDisk();
        return _list.list.GetEnumerator();
    }

    [Serializable]
    private abstract class JsonList<ListT> {
        public List<ListT> list = new List<ListT>();
    } 
}
