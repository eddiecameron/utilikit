/* BehaviourBase.cs
 * ----------------------------
 * Copyright Eddie Cameron & Grasshopper 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Base class for all Singletons, T is the actual type
/// </summary>
/// <typeparam name="T">Actual type of subclass</typeparam>
/// <example>
/// <code>public class MyClass : StaticBehaviour&lt;MyClass&gt; {...}
/// </code>
/// </example>
/// <remarks>
/// Use when there will only be one instance of a script in the scene. Makes access to non-static variables from static methods easy (with instance.fieldName)
/// </remarks>
public class StaticBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    static T _instance;
    static bool noSpawn;

    protected static bool inScene { get { return _instance; } }
    protected static T instance
    {
        get
        {
            if ( !_instance )
                UpdateInstance();
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if ( _instance )
        {
            if ( _instance != this )
            {
                DebugExtras.LogWarning( "Duplicate instance of " + GetType() + " found. Removing " + name + " and gameobject" );
                Destroy( gameObject );
                return;
            }
        }
        else
            UpdateInstance();
    }

    protected void OnDestroy() {
        if ( this == _instance && !noSpawn ) {
            DebugExtras.LogError( "Destroying a singleton! " + this );
            _instance = null;
        }
    }

    protected static bool CheckInstance()
    {
        return instance;
    }

    static void UpdateInstance()
    {
        _instance = GameObject.FindObjectOfType( typeof( T ) ) as T;
        if ( !_instance && !noSpawn )
        {
            DebugExtras.LogWarning( "No object of type : " + typeof( T ) + " found in scene. Creating" );
            _instance = new GameObject( typeof( T ).ToString() ).AddComponent<T>();
        }

        DontDestroyOnLoad( _instance.gameObject );
    }

    void OnApplicationQuit()
    {
        noSpawn = true;
    }
}
