﻿/* PoolSpawner.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PoolSpawnerGameObject : MonoBehaviour {

    public GameObject spawnObjectPrefab;

    [Tooltip( "Maximum objects to have spawned at once, set to -1 for no limit" )]
    public int maxObjects = 50;

    PoolSpawner _spawner;
    PoolSpawner Spawner
    {
        get {
            if ( _spawner == null )
                _spawner = new PoolSpawner( spawnObjectPrefab, maxObjects );
            return _spawner;
        }
    }

    public T SpawnObject<T>( bool asChildOfSpawner = true ) where T : Component {
        var spawned = SpawnObject( asChildOfSpawner );
        if ( spawned == null )
            return null;
        return spawned.GetComponent<T>();
    }

    /// <summary>
    /// Instantiates 
    /// </summary>
    /// <returns>The object.</returns>
    public PoolSpawnable SpawnObject( bool asChildOfSpawner = true ) {
        return SpawnObject( transform.position, transform.rotation, asChildOfSpawner );
    }

    public PoolSpawnable SpawnObject( Vector3 position, Quaternion rotation, bool asChildOfSpawner = true ) {
        return Spawner.SpawnObject( position, rotation, asChildOfSpawner ? transform : null );
    }

    public void ReleaseObjectToPool( PoolSpawnable gameObject ) {
        Spawner.ReleaseObjectToPool( gameObject );
    }

    public static void MakeSpawner( Component prefab, int maxObjects = 50 ) {
        MakeSpawner( prefab.gameObject, maxObjects );
    }

    public static void MakeSpawner( GameObject prefab, int maxObjects = 50 ) {
        PoolSpawnerGameObject newSpawner = new GameObject( prefab.name + "Spawner" ).AddComponent<PoolSpawnerGameObject>();
        newSpawner.spawnObjectPrefab = prefab;
        newSpawner.maxObjects = maxObjects;
    }
}
