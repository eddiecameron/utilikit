﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using UnityObject = UnityEngine.Object;

public static class PoolSpawnerControl {
    const int _DEFAULT_MAX_OBJECTS_PER_POOL = 50;
    static Dictionary<PoolSpawnable, PoolSpawner> _spawnPools = new Dictionary<PoolSpawnable, PoolSpawner>();

    public static T SpawnObjectFromPool<T>( PoolSpawnable prefab, Transform parent, int maxObjectsInPool = -1 ) where T : Component {
        return SpawnObjectFromPool<T>( prefab, parent.position, parent.rotation, parent, maxObjectsInPool );
    }
    public static T SpawnObjectFromPool<T>( PoolSpawnable prefab, Vector3 position, Quaternion rotation, Transform parent = null, int maxObjectsInPool = -1 ) where T : Component {
        return GetPoolForSpawnable( prefab, maxObjectsInPool ).SpawnObject<T>( position, rotation, parent );
    }

    private static PoolSpawner GetPoolForSpawnable( PoolSpawnable prefab, int maxObjectsInPool ) {
        PoolSpawner spawner;
        if ( !_spawnPools.TryGetValue( prefab, out spawner ) ) {
            spawner = new PoolSpawner( prefab, maxObjectsInPool > 0 ? maxObjectsInPool : _DEFAULT_MAX_OBJECTS_PER_POOL );
            _spawnPools.Add( prefab, spawner );
        }
        else if ( maxObjectsInPool > 0 && maxObjectsInPool != spawner.maxObjects )
            spawner.maxObjects = maxObjectsInPool;

        return spawner;
    }
}

public class PoolSpawner {
    public int maxObjects;

    public int Count {
        get {
            return spawned.Count;
        }
    }

    GameObject prefab;
    IndexableHashset<PoolSpawnable> pool = new IndexableHashset<PoolSpawnable>();
    HashSet<PoolSpawnable> spawned = new HashSet<PoolSpawnable>();

    public PoolSpawner( GameObject prefab, int maxObjects ) {
        this.prefab = prefab;
        this.maxObjects = maxObjects;
    }

    public PoolSpawner( Component prefab, int maxObjects ) {
        this.prefab = prefab.gameObject;
        this.maxObjects = maxObjects;
    }

    public T SpawnObject<T>( Transform parent = null ) where T : Component {
        var spawned = SpawnObject( parent );
        if ( spawned == null )
            return null;
        return spawned.GetComponent<T>();
    }

    public T SpawnObject<T>( Vector3 position, Quaternion rotation, Transform parent = null ) where T : Component {
        var spawned = SpawnObject( position, rotation, parent );
        if ( spawned == null )
            return null;
        return spawned.GetComponent<T>();
    }

    /// <summary>
    /// Instantiates
    /// </summary>
    /// <returns>The object.</returns>
    public PoolSpawnable SpawnObject( Transform parent = null ) {
        Vector3 pos = parent == null ? Vector3.zero : parent.position;
        Quaternion rotation = parent == null ? Quaternion.identity : parent.rotation;
        return SpawnObject( pos, rotation, parent );
    }

    public PoolSpawnable SpawnObject( Vector3 position, Quaternion rotation, Transform parent = null ) {
        PoolSpawnable spawnedObject;
        if ( pool.Count > 0 ) {
            spawnedObject = pool[0];
            pool.Remove( spawnedObject );

            spawnedObject.transform.SetParent( parent );
            spawnedObject.transform.position = position;
            spawnedObject.transform.rotation = rotation;
        }
        else if ( maxObjects < 0 || spawned.Count < maxObjects ) {
            var spawnedGO = GameObject.Instantiate( prefab, position, rotation, parent );
            spawnedObject = spawnedGO.GetComponent<PoolSpawnable>();
            if ( spawnedObject == null )
                spawnedObject = spawnedGO.AddComponent<PoolSpawnable>();
            spawnedObject.Spawn( this );
        }
        else {
            DebugExtras.LogWarning( "Pool has reached maximum" );
            return null;
        }

        spawnedObject.gameObject.SetActive( true );
        spawned.Add( spawnedObject );

        return spawnedObject;
    }

    public void ReleaseObjectToPool( PoolSpawnable gameObject ) {
        if ( !spawned.Remove( gameObject ) ) {
            Debug.LogWarning( "Gameobject " + gameObject + " not in pool" );
            return;
        }

        gameObject.gameObject.SetActive( false );
        pool.Add( gameObject );
    }

    public void RemoveFromPool( PoolSpawnable gameObject ) {
        spawned.Remove( gameObject );
        pool.Remove( gameObject );
    }
}

public class IndexableHashset<T> : ICollection<T>, IEnumerable<T>, IEnumerable {
    private Dictionary<T, int> _set;
    private List<T> _list;

    public int Count { get { return _set.Count; } }

    public bool IsReadOnly { get { return false; } }

    public T this[int index]
    {
        get {
            return _list[index]; }
    }

    public IndexableHashset() {
        _set = new Dictionary<T, int>();
        _list = new List<T>();
    }

    public IndexableHashset( int capacity ) {
        _set = new Dictionary<T, int>( capacity );
        _list = new List<T>( capacity );
    }

    public void Add( T item ) {
        if ( _set.ContainsKey( item ) )
            throw new ArgumentException( "Duplicate key " );

        _list.Add( item );
        _set.Add( item, _list.Count - 1 );

    }

    public bool Remove( T item ) {
        int idx;
        if ( !_set.TryGetValue( item, out idx ) )
            return false;

        _set.Remove( item );

        T objToReplace = _list[_list.Count - 1];
        _list.RemoveAt( _list.Count - 1 );

        if ( idx < _list.Count ) {
            _list[idx] = objToReplace;
            _set[objToReplace] = idx;
        }
        return true;
    }

    public void Clear() {
        _set.Clear();
        _list.Clear();
    }

    public bool Contains( T item ) {
        return _set.ContainsKey( item );
    }

    public void CopyTo( T[] array, int arrayIndex ) {
        _list.CopyTo( array, arrayIndex );
    }

    public IEnumerator<T> GetEnumerator() {
        return _list.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return ( (IEnumerable)_list ).GetEnumerator();
    }
}
