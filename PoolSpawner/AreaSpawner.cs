﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathUtils;

[RequireComponent( typeof( PoolSpawnerGameObject ) )]
public class AreaSpawner : MonoBehaviour {
    public Bounds area;

    public int initSpawnAmount;
    public bool randomiseRotation;

    PoolSpawnerGameObject spawner;

    public Vector3 GetPointWithin() {
        return area.RandomPointWithin();
    }

    private void OnDrawGizmosSelected() {
        var curMatrix = Gizmos.matrix;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube( area.center, area.size );
        Gizmos.matrix = curMatrix;
    }

    private void Awake() {
        spawner = GetComponent<PoolSpawnerGameObject>();
    }

    private void Start() {
        for ( int i = 0; i < initSpawnAmount; i++ ) {
            Spawn();
        }
    }

    public T Spawn<T>() where T : Component {
        var spawned = Spawn();
        if ( spawned == null )
            return null;

        return spawned.GetComponent<T>();
    }

    public PoolSpawnable Spawn() {
        Vector3 localOffset = area.RandomPointWithin();
        var spawned = spawner.SpawnObject();
        if ( spawned == null )
            return null;

        spawned.transform.localPosition = localOffset;
        if ( randomiseRotation )
            spawned.transform.localRotation = Random.rotationUniform;

        return spawned;
    }
}
