﻿/* PoolSpawnable.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 *
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PoolSpawnable : MonoBehaviour {
    private PoolSpawner pool;

    private void OnDestroy() {
        if ( pool != null )
            pool.RemoveFromPool( this );
    }

    public void Spawn( PoolSpawner pool ) {
        this.pool = pool;
    }

    public void Release() {
        if ( pool != null )
            pool.ReleaseObjectToPool( this );
        else
            Destroy( gameObject );
    }
}
