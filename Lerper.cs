﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathUtils;
using System;

public static class Lerper {

    public static float EasedLerp(float a, float b, float t, Easer.Equations easeType = Easer.Equations.Linear)
    {
        return (float)(Easer.DoEquation(t, a, b, 1, easeType));
    }

    public static Coroutine StartEase(float duration, Action<float> easeAction, Easer.Equations easeType = Easer.Equations.Linear, Action onComplete = null, MonoBehaviour coroutineOwner = null )
    {
        return StartEase(0, 1, duration, easeAction, easeType, onComplete, coroutineOwner );
    }

    public static Coroutine StartEase(float from, float to, float duration, Action<float> easeAction, Easer.Equations easeType = Easer.Equations.Linear, Action onComplete = null, MonoBehaviour coroutineOwner = null )
    {
        return CoroutineRunner.RunCoroutine(DoEase(from, to, duration, easeAction, easeType, onComplete), coroutineOwner );
    }

    private static IEnumerator DoEase(float from, float to, float duration, Action<float> easeAction, Easer.Equations easeType, Action onComplete) { 
        float t = 0;
        while ( t < 1f )
        {
            easeAction(EasedLerp(from, to, t, easeType));

            yield return 0;
            t += Time.deltaTime / duration;
        }

        easeAction(to);

        if (onComplete != null)
            onComplete();
    }
}
