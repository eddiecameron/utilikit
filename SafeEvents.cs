/* SafeEvents.cs
 * ----------------------------
 * Copyright Eddie Cameron & Grasshopper, 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 * 
 * 
 * 
 */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A collection of methods used to raise events safely.
/// </summary>
/// <remarks>
/// Events raised the conventional way will stop if any listeners throw an exception, so
/// listener functions aren't guaranteed to execute.
/// Using these methods will not only check for null events for you, but will catch exceptions 
/// to make sure all listener functios are run
/// </remarks>
/// <example>
/// Useage - Instead of raising an event like:
/// <code>
/// if ( someEvent != null )
///      someEvent();
/// </code>
/// Raise with:
/// <code>SafeEvents.SafeRaise( someEvent );</code>
/// </example>
public static class SafeEvents
{
    /// <summary>
    /// Raise a zero-parameter event safely
    /// </summary>
    /// <param name="safeEvent"></param>
    public static void SafeRaise( Action safeEvent )
    {
        if ( safeEvent != null )
        {
            Action eventCopy;
            lock ( safeEvent )
            {
                eventCopy = safeEvent;
            }

            foreach ( Action subscriber in eventCopy.GetInvocationList() )
            {
                try
                {
                    subscriber();
                }
                catch ( System.Exception e )
                {
                    DebugExtras.LogError( e );
                }
            }
        }
    }

    /// <summary>
    /// Raise a single paramater event safely
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="safeEvent"></param>
    /// <param name="arg"></param>
    public static void SafeRaise<T>( Action<T> safeEvent, T arg )
    {
        if ( safeEvent != null )
        {
            Action<T> eventCopy;
            lock ( safeEvent )
            {
                eventCopy = safeEvent;
            }

            foreach ( Action<T> subscriber in eventCopy.GetInvocationList() )
            {
                try
                {
                    subscriber( arg );
                }
                catch ( System.Exception e )
                {
                    DebugExtras.LogError( e );
                }
            }
        }
    }

    /// <summary>
    /// Raise a two parameter event safely
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <param name="safeEvent"></param>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>
    public static void SafeRaise<T1, T2>( Action<T1, T2> safeEvent, T1 arg1, T2 arg2 )
    {

        if ( safeEvent != null )
        {
            Action<T1, T2> eventCopy;
            lock ( safeEvent )
            {
                eventCopy = safeEvent;
            }

            foreach ( Action<T1, T2> subscriber in eventCopy.GetInvocationList() )
            {
                try
                {
                    subscriber( arg1, arg2 );
                }
                catch ( System.Exception e )
                {
                    DebugExtras.LogError( e );
                }
            }
        }
    }

    /// <summary>
    /// Raise a three parameter event safely
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <param name="safeEvent"></param>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>
    /// <param name="arg3"></param>
    public static void SafeRaise<T1, T2, T3>( Action<T1, T2, T3> safeEvent, T1 arg1, T2 arg2, T3 arg3 )
    {
        if ( safeEvent != null )
        {
            Action<T1, T2, T3> eventCopy;
            lock ( safeEvent )
            {
                eventCopy = safeEvent;
            }

            foreach ( Action<T1, T2, T3> subscriber in eventCopy.GetInvocationList() )
            {
                try
                {
                    subscriber( arg1, arg2, arg3 );
                }
                catch ( System.Exception e )
                {
                    DebugExtras.LogError( e );
                }
            }
        }
    }

    /// <summary>
    /// Raise a four parameter event safely
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <param name="safeEvent"></param>
    /// <param name="arg1"></param>
    /// <param name="arg2"></param>
    /// <param name="arg3"></param>
    /// <param name="arg4"></param>
    public static void SafeRaise<T1, T2, T3, T4>( Action<T1, T2, T3, T4> safeEvent, T1 arg1, T2 arg2, T3 arg3, T4 arg4 )
    {
        if ( safeEvent != null )
        {
            Action<T1, T2, T3, T4> eventCopy;
            lock ( safeEvent )
            {
                eventCopy = safeEvent;
            }

            foreach ( Action<T1, T2, T3, T4> subscriber in eventCopy.GetInvocationList() )
            {
                try
                {
                    subscriber( arg1, arg2, arg3, arg4 );
                }
                catch ( System.Exception e )
                {
                    DebugExtras.LogError( e );
                }
            }
        }
    }
}