﻿/* Flasher.cs
 * Copyright Eddie Cameron & Grasshopper 2014
 * Open-sourced under the MIT licence
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Flash any renderer a given colour, for a time
/// </summary>
[RequireComponent( typeof( Renderer ) )]
public class Flasher : MonoBehaviour
{
    /// <summary>
    /// Colour to flash
    /// </summary>
    public Color flashColour;
    /// <summary>
    /// How long does each flashed colour last
    /// </summary>
    public float timePerFlash = 0.2f;

    Material origMat; // to make sure mat doesn't change
    Material flashMat;
    Color origColour;
    bool flashing;

    private Renderer _renderer;
    public Renderer Renderer {
        get {
            if ( _renderer == null )
                _renderer = GetComponent<Renderer>();
            return _renderer;
        }
    }

    /// <summary>
    /// Flash this object for given time
    /// </summary>
    /// <param name="forTime"></param>
    public void Flash( float forTime )
    {
        if ( flashing )
        {
            StopCoroutine( "_Flash" );
        }
        else
        {
            flashing = true;
            origMat = Renderer.material;
            Renderer.sharedMaterial = flashMat = new Material( origMat );
            origColour = origMat.color;
        }
        StartCoroutine( "_Flash", forTime );
    }

    IEnumerator _Flash( float forTime )
    {
        float flashTime = 0f;
        bool flashOn = false;
        while ( flashTime < forTime )
        {
            flashOn = !flashOn;
            flashMat.color = flashOn ? flashColour : origColour;
            yield return new WaitForSeconds( timePerFlash );
            flashTime += timePerFlash;
            if ( Renderer.sharedMaterial != flashMat )
            {
                DebugExtras.Log( "Mat changed" );
                flashing = false;
                yield break;
            }
        }

        flashing = false;
        Renderer.material = origMat;
    }
}
