﻿/* SingleLerper.cs
 * Copyright Eddie Cameron 2013
 * ----------------------------
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingleLerper : MonoBehaviour 
{
    [SerializeField]
    float _curValue;
    public float lerpSpeed = 1f; // how far can value change in 1 sec

    private float _value_;
    public float Value {
        get { return _value_; }
        set {
            _value_ = lastValue = value;
            curLerpAmount = Time.deltaTime * lerpSpeed / Mathf.Abs (Target - lastValue);
            enabled = true;
        }
    }

    private float _target_;
    public float Target {
        get { return _target_; }
        set {
            _target_ = value;
            lastValue = Value;
            curLerpAmount = Time.deltaTime * lerpSpeed / Mathf.Abs (value - lastValue);
            enabled = true;
        }
    }

    float curLerpAmount = 1f;
    float lastValue;

    protected void Awake() {
        lastValue = Value = _curValue;
    }

    protected void Update() {
        if ( curLerpAmount < 1f && !Mathf.Approximately( Target, lastValue ) ) {
            _value_ = Mathf.Lerp( lastValue, Target, curLerpAmount );
            curLerpAmount += Time.deltaTime * lerpSpeed / Mathf.Abs( Target - lastValue );
        }
        else {
            _value_ = Target;
            enabled = false;
        }
    }

    #region Static Creator
    static Dictionary<GameObject, SingleLerper> lerpers = new Dictionary<GameObject, SingleLerper>();

    public static SingleLerper SetTargetLerp( GameObject onObject, float target ) {
        SingleLerper lerper;
        if ( !lerpers.TryGetValue( onObject, out lerper ) ) {
            lerper = onObject.AddComponent<SingleLerper>();
            lerpers.Add( onObject, lerper );
        }
        lerper.Target = target;

        return lerper;
    }
    #endregion
}