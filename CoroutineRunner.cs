﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineRunner : StaticBehaviour<CoroutineRunner> {
    public static Coroutine RunCoroutine( IEnumerator coroutine, MonoBehaviour coroutineOwner = null )
    {
        if ( coroutineOwner == null )
            return instance.StartCoroutine( coroutine );
        else
            return coroutineOwner.StartCoroutine( coroutine );
    }

    public static void StopRunningCoroutine( Coroutine coroutine )
    {
        instance.StopCoroutine(coroutine);
    }
}
